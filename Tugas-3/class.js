class Animal {
    constructor(name) {
        this.serigala = name ;
        this.kucing = 4 ;
        this.burung = false;
} get name() {
    return this.serigala;
} get legs() {
    return this.kucing;
} get cold_blooded() {
    return this.indonesia;
} set name(x) {
    this.serigala = x;
} set legs(y) {
    this.kucing = y;
} set cold_blooded(z) {
    this.burung = false;
}
}
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.serigala = name;
        this.kucing = 2;
    }
    yell() { return console.log("Auooo"); }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.serigala = name;
    }
    jump() { return console.log("hop hop"); }
}
    
    

let sheep = new Animal("shaun");
console.log(sheep.serigala) // "shaun"
console.log(sheep.kucing) // 4
console.log(sheep.burung) // false
console.log("");

let sungokong = new Ape("kera sakti")
console.log(sungokong.serigala); //"kera sakti"
console.log(sungokong.kucing); //2
console.log(sungokong.burung) // false
sungokong.yell() // "Auooo"
console.log("");

let kodok = new Frog("buduk")
console.log(kodok.serigala); //buduk
console.log(kodok.kucing); //4
console.log(kodok.burung) // false
kodok.jump() // "hop hop"
console.log("");

//02
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();